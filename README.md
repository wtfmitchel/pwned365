# pwned365
    <#
            .SUMMARY
            This script queries an entire Office 365 tenant against the https://haveibeenpwned.com API service 
            graciously created by Troy Hunt (@troyhunt) and writes all hits into an excel spreadsheet in order to highlight
            accounts that may be threatened or compromised. 
            
            .RQUIREMENTS
            PS C:\Users\mitchel> .\pwned.ps1

            .EXAMPLE
            PS C:\Users\mitchel> .\pwned.ps1

 
            .NOTES
            Author:  Mitchel Lewis
            Website: https://medium.com/@wtfmitchel
            Twitter: @statustic
            
            .DISCLAIMER
            USE AT YOUR OWN RISK! Always review scripts prior to running them in production; 
            especially if they were written by me!
    #>